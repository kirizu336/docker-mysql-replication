![img](http://blog.ditullio.fr/wp-content/uploads/2016/07/mysql_components-1-300x158.png)

## MYSQL Replication
My Database setup for high availability. Based on this article:

http://blog.ditullio.fr/2016/07/07/docker-replicated-mysql-haproxy-load-balancing/

but with proxysql from this repo:

https://hub.docker.com/r/proxysql/proxysql

## HOW TO RUN
Make sure your `docker` and `docker-compose` command can run without `sudo` at first. If not, please read this: https://stackoverflow.com/a/67759683/7869984

After that, run `./setup.sh`.

If sometimes you stop the containers, you need to rebuild again! with running `./setup.sh`,, otherwise the replication will not work

## NOTE FOR WINDOWS USER
please use `cmder` or `git bash` for execute setup script

## To follow log files of databases, open other window and execute the bellow commands:

* logging dbmaster
```bash
docker-compose exec dbmaster sh -c 'tail -f /var/log/mysql/*.log'
```

* logging dbslave
```bash
docker-compose exec dbslave sh -c 'tail -f /var/log/mysql/*.log'
```

* logging dbslave 2
```bash
docker-compose exec dbslave_1 sh -c 'tail -f /var/log/mysql/*.log'
```

## you can operate "CREATE TABLE" and "DROP TABLE" query through DBMASTER (PORT 13306).

```bash
$ mysql -h 127.0.0.1 -uroot -p -P 13306 appdb -e "use appdb; create table users(id int(10) AUTO_INCREMENT PRIMARY KEY, username varchar(50));"
Enter password: (default root)
```

## you can operate "SELECT" query through ProxySQL (PORT 6033).

```bash
$ mysql -h 127.0.0.1 -u proxysql -p -P 6033 appdb -e"SELECT * FROM users;"
Enter password: (default pass)

+----+-----------------------+
| id | name                  |
+----+-----------------------+
|  1 | kirz336               |
+----+-----------------------+
```

* dbslave logs must be like:
```bash
2021-11-28T01:59:39.848947Z         1 Query     select * from users
```

## you can operate "INSERT" query through ProxySQL (PORT 6033)

```bash
$ mysql -h 127.0.0.1 -u proxysql -p -P 6033 appdb -e"INSERT INTO users(username) VALUES ('dodycode12');"
```

* dbmaster logs must be like:
```bash
2021-11-28T02:00:22.770045Z        1 Query     INSERT INTO users VALUES ('dodycode12')
```

## ProxySQL ADMIN MONITOR (PORT 6032)

```
$ mysql -h 127.0.0.1 -u proxysql -p -P 6032
Enter password: (default: pass)
```

* Tables

```
mysql> show tables;
+----------------------------------------------------+
| tables                                             |
+----------------------------------------------------+
| global_variables                                   |
| mysql_aws_aurora_hostgroups                        |
| mysql_collations                                   |
| mysql_firewall_whitelist_rules                     |
| mysql_firewall_whitelist_sqli_fingerprints         |
| mysql_firewall_whitelist_users                     |
| mysql_galera_hostgroups                            |
| mysql_group_replication_hostgroups                 |
| mysql_query_rules                                  |
| mysql_query_rules_fast_routing                     |
| mysql_replication_hostgroups                       |
| mysql_servers                                      |
| mysql_users                                        |
| proxysql_servers                                   |
| restapi_routes                                     |
| runtime_checksums_values                           |
| runtime_global_variables                           |
| runtime_mysql_aws_aurora_hostgroups                |
| runtime_mysql_firewall_whitelist_rules             |
| runtime_mysql_firewall_whitelist_sqli_fingerprints |
| runtime_mysql_firewall_whitelist_users             |
| runtime_mysql_galera_hostgroups                    |
| runtime_mysql_group_replication_hostgroups         |
| runtime_mysql_query_rules                          |
| runtime_mysql_query_rules_fast_routing             |
| runtime_mysql_replication_hostgroups               |
| runtime_mysql_servers                              |
| runtime_mysql_users                                |
| runtime_proxysql_servers                           |
| runtime_restapi_routes                             |
| runtime_scheduler                                  |
| scheduler                                          |
+----------------------------------------------------+
32 rows in set (0.00 sec)
```

* mysql_servers

```
mysql> SELECT * FROM mysql_servers;
+--------------+---------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| hostgroup_id | hostname      | port | gtid_port | status | weight | compression | max_connections | max_replication_lag | use_ssl | max_latency_ms | comment |
+--------------+---------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
| 0            | 192.168.0.201 | 3306 | 0         | ONLINE | 1      | 0           | 1000            | 5                   | 0       | 0              |         |
| 1            | 192.168.0.202 | 3306 | 0         | ONLINE | 1      | 0           | 1000            | 5                   | 0       | 0              |         |
| 1            | 192.168.0.203 | 3306 | 0         | ONLINE | 1      | 0           | 1000            | 5                   | 0       | 0              |         |
+--------------+---------------+------+-----------+--------+--------+-------------+-----------------+---------------------+---------+----------------+---------+
3 rows in set (0.01 sec)
```


* monitoring backend databases connection.

```
mysql> SELECT * FROM monitor.mysql_server_connect_log ORDER BY time_start_us DESC LIMIT 10;
+---------------+------+------------------+-------------------------+---------------+
| hostname      | port | time_start_us    | connect_success_time_us | connect_error |
+---------------+------+------------------+-------------------------+---------------+
| 192.168.0.203 | 3306 | 1635949658095360 | 774                     | NULL          |
| 192.168.0.202 | 3306 | 1635949658076311 | 667                     | NULL          |
| 192.168.0.201 | 3306 | 1635949658062632 | 1365                    | NULL          |
+---------------+------+------------------+-------------------------+---------------+
3 rows in set (0.01 sec)
```

* mysql_replication_hostgroups

```
ProxySQL> SELECT * FROM mysql_replication_hostgroups;
+------------------+------------------+-------------+
| writer_hostgroup | reader_hostgroup | comment     |
+------------------+------------------+-------------+
| 0                | 1                | host groups |
+------------------+------------------+-------------+
1 row in set (0.00 sec)
```