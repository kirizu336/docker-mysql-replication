docker-compose down -v

docker-compose up -d dbmaster

sleep 3

docker-compose up -d dbslave

sleep 3

docker-compose up -d dbslave_1

echo "Waiting for all databases created..."

sleep 10

echo "Copying init scripts for db"

docker cp ./scripts/init.sh dbmaster:/app/init.sh
docker cp ./scripts/init.sh dbslave:/app/init.sh
docker cp ./scripts/init.sh dbslave_1:/app/init.sh

sleep 5

echo "initialize database certificates"

sleep 2

echo "Init db master"

if docker ps -f name=dbmaster | grep dbmaster; then
    docker exec -ti dbmaster bash init.sh
fi

sleep 5

echo "Init db slave"

if docker ps -f name=dbslave | grep dbslave; then
    docker exec -ti dbslave bash init.sh
fi

sleep 5

echo "Init db slave 2"

if docker ps -f name=dbslave_1 | grep dbslave_1; then
    docker exec -ti dbslave_1 bash init.sh
fi

echo "Finished.."

sleep 3

echo "Restarting Databases..."

docker-compose restart dbmaster

sleep 3

docker-compose restart dbslave

sleep 3

docker-compose restart dbslave_1

sleep 5

echo "Database Replication Started"

if docker ps -f name=dbslave_1 | grep dbslave_1; then
    docker exec -ti dbmaster bash replication-start.sh 192.168.0.201 192.168.0.202 192.168.0.203
else
    sleep 10
    docker exec -ti dbmaster bash replication-start.sh 192.168.0.201 192.168.0.202 192.168.0.203
fi

echo "Database Replication Finished"

sleep 1

echo "Creating proxysql users..."

sleep 5

if docker ps -f name=dbmaster | grep dbmaster; then
    docker exec -ti dbmaster mysql -uroot -proot appdb -e "FLUSH PRIVILEGES; CREATE USER 'proxysql'@'%' IDENTIFIED BY 'pass'; CREATE USER 'haproxy_check'@'%';"
    sleep 5
    docker exec -ti dbmaster mysql -uroot -proot appdb -e "GRANT ALL ON *.* to 'proxysql'@'%' IDENTIFIED BY 'pass'; GRANT ALL ON *.* to 'haproxy_check'@'%';"
fi

if docker ps -f name=dbslave | grep dbslave; then
    sleep 5
    docker exec -ti dbmaster mysql -uroot -proot appdb -e "GRANT ALL ON *.* to 'proxysql'@'%' IDENTIFIED BY 'pass'; GRANT ALL ON *.* to 'haproxy_check'@'%';"
fi

if docker ps -f name=dbslave_1 | grep dbslave_1; then
    sleep 5
    docker exec -ti dbmaster mysql -uroot -proot appdb -e "GRANT ALL ON *.* to 'proxysql'@'%' IDENTIFIED BY 'pass'; GRANT ALL ON *.* to 'haproxy_check'@'%';"
fi

echo "ProxySQL users created..."

sleep 2

echo "Creating ProxySQL..."

if docker ps -f name=dbmaster | grep dbmaster; then
    docker-compose up -d loadbalancer
fi

echo "Proxy Setup Finished. Please check README files for more details guide"